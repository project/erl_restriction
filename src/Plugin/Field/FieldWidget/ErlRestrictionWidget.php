<?php

namespace Drupal\erl_restriction\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Layout\LayoutPluginManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormFactoryInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\entity_reference_layout\Plugin\Field\FieldWidget\EntityReferenceLayoutWidget;

/**
 * Entity Reference with Layout field widget.
 *
 * @FieldWidget(
 *   id = "erl_restriction_widget",
 *   label = @Translation("Entity reference layout (With layout builder and
 *   restriction)"), description = @Translation("Layout builder for
 *   paragraphs."), field_types = {
 *     "entity_reference_layout_revisioned"
 *   },
 * )
 */
class ErlRestrictionWidget extends EntityReferenceLayoutWidget implements ContainerFactoryPluginInterface {

  protected $paragraphs;

  protected $restrictions;

  protected $fieldSettings;

  function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    Renderer $renderer,
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    LayoutPluginManager $layout_plugin_manager,
    PluginFormFactoryInterface $plugin_form_manager,
    LanguageManager $language_manager,
    AccountProxyInterface $current_user,
    EntityDisplayRepositoryInterface $entity_display_repository) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $third_party_settings,
      $renderer,
      $entity_type_manager,
      $entity_type_bundle_info,
      $layout_plugin_manager,
      $plugin_form_manager,
      $language_manager,
      $current_user,
      $entity_display_repository);

    $this->paragraphs = $this->entityTypeBundleInfo->getBundleInfo('paragraph');
    $this->restrictions = $this->getSetting('restrictions');
    $this->fieldSettings = $this->fieldDefinition->getSetting('handler_settings');
  }

  /**
   * {@inheritDoc}
   */
  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {
    $elements = parent::formMultipleElements($items, $form, $form_state);

    foreach ($elements as $key => $layoutArray) {
      if (!is_numeric($key) || $layoutArray['layout']['#value'] == "" || !isset($layoutArray['#layout']) && $this->restrictions[$layoutArray['#layout']]) {
        continue;
      }

      $layout_name = ($layoutArray['#layout'] === "") ? $layoutArray['layout']['#value'] : $layoutArray['#layout'];
      $regions = $this->restrictions[$layout_name];

      if (is_null($regions)) {
        continue;
      }

      $allowedParagraphsFieldSettings = $this->getTargetBundlesWithoutLayoutBundles();

      $is_none_selected = $this->isNoneSelected($regions);
      $allowed = [];

      // If none is selected, get all paragraphs available for this layout and put it into the allowed array.
      if ($is_none_selected) {
        $allowed = $this->getParagraphLabelsFromKeys($allowedParagraphsFieldSettings);
      }

      foreach ($regions as $region_key => $region) {
        if (!$is_none_selected) {
           $selected = $this->getSelectedParagraphs($this->restrictions[$layout_name][$region_key]);

          // Allowed in both, the field and specific region.
          $allowedInBoth = array_intersect($selected, $allowedParagraphsFieldSettings);
          $allowed = $this->getParagraphLabelsFromKeys($allowedInBoth);
        }

        $this->appendFormArrayWithAllowed($elements, $allowed, $key, $region_key);
      }
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $defaults = parent::defaultSettings();
    $defaults += [
      'restrictions' => [],
    ];

    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $options = $this->getParagraphLabelsFromKeys($this->getTargetBundlesWithoutLayoutBundles());

    $form['restrictions'] = [];

    $form['restrictions']['blacklist'] = [
      '#type' => 'radios',
      '#title' => $this->t('Which paragraph types should be allowed?'),
      '#options' => [
        'include' => $this->t('Include the selected below'),
        'exclude' => $this->t('Exclude the selected below'),
      ],
      '#default_value' => $this->getSetting('restrictions')['blacklist'],
    ];

    $definitions = $this->layoutPluginManager->getDefinitions();

    foreach ($this->fieldSettings['allowed_layouts'] as $allowed_layout_type) {
      foreach ($allowed_layout_type as $layout_key => $layout_name) {

        $form['restrictions'][$layout_key] = [
          '#type' => 'fieldset',
          '#title' => $layout_name,
        ];

        foreach ($definitions[$layout_key]->getRegions() as $region_key => $region) {
          $form['restrictions'][$layout_key][$region_key] = [
            '#type' => 'checkboxes',
            '#title' => $region['label'],
            '#options' => $options,
            '#default_value' => $this->restrictions[$layout_key][$region_key],
          ];
        }
      }
    }
    return $form;
  }

  protected function getTargetBundlesWithoutLayoutBundles() {
    $targetBundles = $this->fieldSettings['target_bundles'];

    foreach ($this->fieldSettings['layout_bundles'] as $layoutBundle) {
      if (isset($targetBundles[$layoutBundle])) {
        unset($targetBundles[$layoutBundle]);
      }
    }
    return is_null($targetBundles) ? [] : $targetBundles;
  }

  /**
   * Checks if none of the paragraphs in a region is selected.
   *
   * @param array $layout
   *
   * @return bool
   */
  protected function isNoneSelected(array $layout) {
    $selected = [];
    foreach ($layout as $region) {
      foreach ($region as $value_key => $value) {
        if ($value !== 0) {
          $selected[] = $value_key;
        }
      }
      if (count($selected) > 0) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Gets the paragraph label for an array of paragraph keys and creates a
   * key=>label array.
   *
   * @param array $keys
   *
   * @return array
   */
  protected function getParagraphLabelsFromKeys(array $keys) {
    $withLabel = [];
    foreach ($keys as $key) {
      $withLabel[$key] = $this->paragraphs[$key]['label'];
    }
    return $withLabel;
  }

  /**
   * Returns an array with all selected paragraphs for a region in it.
   *
   * @param array $values
   *
   * @return array
   */
  protected function getSelectedParagraphs(array $values) {
    $selected = [];

    // Should the selected paragraphs be excluded or included.
    $blacklist = $this->restrictions['blacklist'];

    foreach ($values as $value_key => $value) {
      // If the selected values get excluded, the value must be zero.
      if ($blacklist === "exclude" && $value === 0) {
        $selected[] = $value_key;
      }
      // If the selected values get included, the value must not be zero.
      elseif ($blacklist === "include" && $value !== 0) {
        $selected[] = $value_key;
      }
    }

    return $selected;
  }

  /**
   * Appends a Form API compliant array with all the selected paragraphs in it
   * to the elements variable.
   *
   * @param array $allowed
   * @param array $elements
   * @param $key
   * @param $region_key
   *
   */
  protected function appendFormArrayWithAllowed(array &$elements, array $allowed, $key, $region_key) {
    $elements[$key][$region_key]['allowed'] = [
      '#type' => 'select',
      '#options' => $allowed,
      '#attributes' => [
        'class' => ['erl-item-type', 'hidden'],
        'id' => $elements["#field_name"] . '--item-' . $key . '-' . $region_key . '-select',
      ],
    ];
  }
}
